---
title: Join Plasma Bigscreen
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
  - scss/join.scss
---

If you'd like to contribute to the amazing free software for TV's and setup-boxes, [join us - we always have a task for you](/contributing/)!

### Plasma Bigscreen specific channels:

* [![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasma-bigscreen:kde.org) 

* [![](/img/telegram.svg)Telegram](https://t.me/plasma_bigscreen)


### Plasma Biscreen related project channels:

* [![](/img/irc.png)#plasma on Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)

* [![](/img/mail.svg)Plasma development mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel)
