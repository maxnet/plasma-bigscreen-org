# Plasma Bigscreen Website

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_plasma-bigscreen-org)](https://binary-factory.kde.org/job/Website_plasma-bigscreen-org/)

This is the git repository for [plasma-bigscreen.org](https://plasma-bigscreen.org), the website for Plasma Bigscreen.

As a (Hu)Go module, it requires both Hugo and Go to work.

### Development
Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/)

### I18n
See [hugoi18n](https://invent.kde.org/websites/hugo-i18n)
