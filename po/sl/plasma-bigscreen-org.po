#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2021, 2022.
# Martin Srebotnjak <miles@filmsi.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: websites-plasma-bigscreen-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2023-10-07 07:05+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 3.3.2\n"

#: config.yaml:0
msgid "A privacy-respecting, open source and secure TV ecosystem"
msgstr "Ekosistem, ki spoštuje zasebnost, odprto kodo in varno TV"

#: config.yaml:0
msgid "Project"
msgstr "Projekt"

#: config.yaml:0
msgid "Documentation"
msgstr "Dokumentacija"

#: config.yaml:0
msgid "Community"
msgstr "Skupnost"

#: config.yaml:0
msgid "Apps"
msgstr "Aplikacije"

#: content/_index.md:0 i18n/en.yaml:0
msgid "Plasma Bigscreen"
msgstr "Plasma Bigscreen"

#: content/faq.md:0
msgid "Frequently Asked Questions"
msgstr "Pogosto zastavljena vprašanja"

#: content/faq.md:0
msgid "FAQ"
msgstr "PZV"

#: content/faq.md:12
msgid "Help! My TV remote does not work, how do I fix it?"
msgstr "Na pomoč! Moj TV daljinec ne deluje, kako naj ga popravim?"

#: content/faq.md:14
msgid ""
"Many TV manufacturers require HDMI-CEC mode to be enabled manually, one "
"needs to refer to the TV user guide to figure out how to configure the HDMI-"
"CEC option on their television set. The HDMI-CEC option can be found under "
"various names depending on the TV manufacturers, some for example: (TCL TV: "
"T-Link, Panasonic TV: Viera Link, Samsung TV: Anynet+, Sony TV: Bravia Sync)."
msgstr ""
"Mnogi proizvajalci TV zahtevajo, da je za ročno omogočanje načina HDMI-CEC "
"potrebno pogledati v navodila za uporabo TV, da bi ugotovili, kako "
"prilagoditi možnost HDMI-CEC na svojem televizijskem sprejemnik. Možnost "
"HDMI-CEC je na voljo pod različnimi imeni, odvisno od proizvajalcev "
"televizorjev, npr.: (TCL TV: T-Link, Panasonic TV: Viera Link, Samsung TV: "
"Anynet+, Sony TV: Bravia Sync)."

#: content/faq.md:17
msgid ""
"Also make sure your Plasma Bigscreen device is actually supported by libcec, "
"[check their supported hardware](https://github.com/Pulse-Eight/"
"libcec#supported-hardware)."
msgstr ""
"Prepričajte se, da je naprava Plasma Bigscreen zares podprta s libcec, "
"[preverite njihovo podprto strojno opremo](https://github.com/Pulse-Eight/"
"libcec#supported-hardware)."

#: content/faq.md:19
msgid ""
"Some buttons on my TV remote are not working, I can’t exit an application or "
"use the back button properly. How do I fix it?"
msgstr ""
"Nekateri gumbi na mojem daljinskem upravljavcu televizorja ne delujejo, ne "
"morem zapustiti aplikacije oz. ne morem uporabljati gumba nazaj. Kako naj to "
"popravim?"

#: content/faq.md:21
msgid ""
"HDMI-CEC on the beta image is in a testing phase. We have only been able to "
"test the functionality on a few selected range of TV sets and have mapped TV "
"remotes on the basis of those working devices. You can map and test your TV "
"remote following a few simple steps of debugging and editing files listed "
"below."
msgstr ""
"HDMI-CEC v beta podobi je v fazi testiranja. Funkcionalnost smo lahko "
"preskusili samo na nekaj izbranih televizorjih in na podlagi teh delovnih "
"naprav smo testirali preslikane televizijske daljince. Preslikavate in "
"preizkusite televizor lahko na daljavo po, če sledite nekaj preprostih "
"korakih razhroščanja in urejanja datotek, navedenih spodaj."

#: content/faq.md:25
msgid "_Test if the KEY is working with HDMI-CEC and extract it’s KEY CODE:_"
msgstr "_Preveri ali tipka deluje z HDMI-CEC in izlušči KODO TIPKE:_"

#: content/faq.md:32
msgid ""
"Once the script is running, press the button on your TV remote to extract "
"its KEY CODE, if no KEY CODE is found the KEY might not be part of HDMI-CEC "
"controls enabled on your TV set, refer to the TV User Guide to know which "
"keys are enabled under your TV manufacturers HDMI-CEC implementation."
msgstr ""
"Ko se skript izvaja, pritisnite tipko na daljinskem upravljalniku "
"televizorja, da izvlečete KODO TIPKE, če ne najdete KODE TIPKE, koda morda "
"ni del kontrolnikov HDMI-CEC, ki so omogočeni na vašem televizorju, poglejte "
"navodila za uporabo televizorja, da bi izvedeli katere tipke so omogočene "
"pri televizorjih HDMI-CEC."

#: content/faq.md:34
msgid "_Adding the found KEY CODE and mapping it in the CEC daemon:_"
msgstr ""
"_Dodajanje najdene KODE TIPKE in njene preslikovanje v skriti proces CEC:_"

#: content/faq.md:41
msgid "Locate KEYMAP = {} in the daemon script"
msgstr "Locirajte KEYMAP = {} v skriptu demona"

#: content/faq.md:42
msgid ""
"Add your KEY CODE in the following format to the list: “9: u.KEY\\_HOMEPAGE”"
msgstr ""
"Na seznam dodajte KODO TIPKE v naslednji obliki: \"9: u.KEY\\_HOMEPAGE\""

#: content/faq.md:43
msgid ""
"In the above example “u.KEY\\_HOMEPAGE” is mapped to the home button that is "
"used to exit an application"
msgstr ""
"V zgornjem primeru je »u.KEY\\_HOMEPAGE\" preslikan na tipko domov, ki se "
"uporablja za izhod iz aplikacije"

#: content/faq.md:44
msgid "“9” being the Key Code"
msgstr "»9« je koda tipke"

#: content/faq.md:45
msgid "“u.KEY\\_HOMEPAGE” being the action the key should perform"
msgstr "\"u.KEY\\_HOMEPAGE\" je dejanje, ki bi ga morala izvesti tipka"

#: content/faq.md:47
msgid ""
"I have a generic USB remote but it’s missing the home key, how do I exit "
"applications?"
msgstr ""
"Imam generičnega USB daljinca, vendar mu manjka tipka domov, kako naj "
"izstopim iz aplikacij?"

#: content/faq.md:49
msgid ""
"Not all generic USB remotes are built alike, therefore we recommend using a "
"tested product like the “Wechip G20 Air Mouse With Microphone Remote”. If in-"
"case you are unable to get your hands on one, you can still map an existing "
"key on the remote."
msgstr ""
"Vsi generični daljinci USB niso izdelani podobno, zato priporočamo uporabo "
"preizkušenega izdelka, kot je »Wechip G20 Air Mouse With Microphone Remote«. "
"V primeru, da ne morete priti do njega, lahko še vedno preslikate obstoječo "
"tipko na daljinskem upravljalniku."

#: content/faq.md:52
msgid "_Mapping the window close button to a button on a USB remote:_"
msgstr ""
"_Preslikava gumba za zapiranje okna v gumb na daljinskem upravljalniku USB:_"

#: content/faq.md:59
msgid "Find the entry “Window Close” located under `[Kwin]`"
msgstr "Poiščite vnos »Zapri okno« pod »[Kwin]«"

#: content/faq.md:60
msgid "Assign your button to the “Window Close” entry"
msgstr "Dodelite gumb vnosu »Zapri okno«"

#: content/faq.md:61
msgid ""
"Example: `Window Close=Alt+F4\\t’YourButtonHere’,Alt+F4\\t’YourButtonHere’,"
"Close Window`"
msgstr ""
"Primer: `Zapri okno=Alt+F4\\t’Tule je vaš gumb’,Alt+F4\\t'Vaš gumb tukaj’,"
"Zapri okno`"

#: content/faq.md:63
msgid ""
"Voice applications do not start on boot and I see a blank screen. What "
"should I do?"
msgstr ""
"Glasovne aplikacije se ne zaženejo ob zagonu in vidim prazen zaslon. Kaj naj "
"naredim?"

#: content/faq.md:65
msgid ""
"Voice applications are only accessible once MyCroft is ready and has "
"started, you will be notified on the top panel when MyCroft has started and "
"is in a ready state."
msgstr ""
"Glasovne aplikacije so dostopne le, ko je MyCroft pripravljen in se je "
"zagnal; obveščeni boste na zgornji plošči, ko se je myCroft zagnal in je v "
"stanju pripravljenosti."

#: content/faq.md:67
msgid "How do I exit applications using a external keyboard?"
msgstr "Kako naj zapustim aplikacijo z uporabo zunanje tipkovnice?"

#: content/faq.md:69
msgid ""
"“Alt+F4” is the general shortcut assigned to closing applications using a "
"external keyboard. Custom keys can be assigned to the following file for "
"various actions:"
msgstr ""
"“Alt+F4” je splošna bližnjica določena za zapiranje aplikacij z uporabo "
"zunanje tipkovnice. Tipke po meri so lahko določene za naslednjo datoteko za "
"različne dejavnosti:"

#: content/faq.md:77
msgid ""
"What is the hot key assigned to activate MyCroft voice control without a USB "
"mic remote?"
msgstr ""
"Katera je hitra tipka, dodeljena aktiviranju glasovnega nadzora MyCroft brez "
"daljinca USB z mikrofonom?"

#: content/faq.md:79
msgid ""
"There is no hotkey assigned to MyCroft, the Generic USB remotes that have a "
"mic button only activate the mic input on the USB remote hardware, to use "
"MyCroft one needs to activate it with the hot word “Hey Mycroft”."
msgstr ""
"Ni hitre tipke, dodeljene MyCroftu, Generični daljinci USB, ki imajo gumb za "
"mikrofon, samo aktivirajo mikrofonski vhod na oddaljeni strojni opremi USB, "
"za uporabo MyCrofta pa ga morate aktivirati s ključno besedo »Hej, Mycroft«."

#: content/faq.md:81
msgid ""
"To use MyCroft one does not require a USB mic enabled remote specifically, "
"any microphone that can be connected to your device should work."
msgstr ""
"Če želite uporabljati MyCroft, za to ni potreben daljinski upravljalnik USB "
"z mikrofonom, deluje vsak mikrofon, ki ga je mogoče priključiti na vašo "
"napravo."

#: content/faq.md:83
msgid ""
"For issues and troubleshooting your microphone not working one can refer to: "
"https://mycroft-ai.gitbook.io/docs/using-mycroft-ai/troubleshooting/audio-"
"troubleshooting."
msgstr ""
"Za težave in odpravljanje težav z mikrofonom, ki ne deluje, si lahko "
"ogledate: https://mycroft-ai.gitbook.io/docs/using-mycroft-ai/"
"troubleshooting/audio-troubleshooting."

#: content/faq.md:85
msgid "How to contribute & upload your custom keymap for CEC?"
msgstr "Kako prispevati in naložiti svojo preslikavo tipk po meri za CEC?"

#: content/faq.md:87
msgid ""
"Currently the development repository of the CEC daemon can be found at "
"https://invent.kde.org/adityam/easycec with instructions on how to add a "
"device with custom keymap."
msgstr ""
"Trenutno razvojno skladišče skrivnega procesa CEC lahko najdete na https://"
"invent.kde.org/adityam/easycec z navodili, kako dodati napravo s preslikavo "
"tipk po meri."

#: content/faq.md:89
msgid "Can Android apps work on Plasma Bigscreen?"
msgstr "Ali lahko aplikacije Android delujejo na Plasma Bigscreen?"

#: content/faq.md:91
msgid ""
"There are projects out there like [Anbox](https://anbox.io/) which is "
"Android running inside a Linux container, and use the Linux kernel to "
"execute applications to achieve near-native performance. This could be "
"leveraged in the future to have Android apps running on top of a GNU/Linux "
"system with the Plasma Bigscreen platform but it's a complicated task, and "
"as of *today* (August 5th, 2021) some distributions already support Anbox "
"and you can run Plasma Bigscreen on top of those distributions."
msgstr ""
"Obstajajo projekti, kot je [Anbox](https://anbox.io/), ki je Android, ki "
"teče znotraj vsebnika Linux, in uporabljajo jedro Linuxa za izvajanje "
"aplikacij za doseganje skoraj domorodne uspešnosti. To bi lahko izkoristili "
"v prihodnosti, da bi aplikacije Android tekle na sistemih GNU/Linux sistema "
"s platformo Plasma Bigscreen, vendar je to zapletena naloga, in sredi leta "
"2021 nekatere distribucije že podpirajo Anbox in lahko zaženete Plasma "
"Bigscreen na teh distribucijah."

#: content/faq.md:94
msgid "Can I run Plasma Bigscreen on my TV or setup-box?"
msgstr ""
"Lahko zaženem Plasma Bigscreen na svojem televizorju ali digitalnem "
"sprejemniku?"

#: content/faq.md:96
msgid "In theory, yes. In practice, it depends."
msgstr "V teoriji, da. V praksi pa je odvisno."

#: content/faq.md:99
msgid ""
"Hardware support is not dictated by Plasma Bigscreen, instead it depends on "
"the devices supported by the distributions shipping it. See [the install "
"page](/get/) for more details of distributions shipping Plasma Bigscreen."
msgstr ""
"Podpora strojni opremi ni vodena s Plasma Bigscreen, ampak je odvisna od "
"naprav, ki so podprte od distributerja, ki jo je dobavil. Poglejte [stran "
"namestitve](/get/) za več podrobnosti o distribucijah, ki dobavljajo Plasma "
"Bigscreen."

#: content/faq.md:102
msgid "What's the state of the project?"
msgstr "Kakšno je stanje projekta?"

#: content/faq.md:104
msgid ""
"Plasma Bigscreen is currently under heavy development and is not intended to "
"be used as a daily driver."
msgstr ""
"Plazma Bigscreen je trenutno sredi razvoja in ni namenjen vsakodnevni "
"uporabi."

#: content/get.md:0 i18n/en.yaml:0
msgid "Install"
msgstr "Namesti"

#: content/get.md:0
msgid "Distributions offering Plasma Bigscreen"
msgstr "Distribucije, ki ponujajo Plasma Bigscreen"

#: content/get.md:11
msgid "postmarketOS"
msgstr "postmarketOS"

#: content/get.md:13
msgid "![](/img/pmOS.svg)"
msgstr "! [](/img/pmOS.svg)"

#: content/get.md:15
msgid ""
"PostmarketOS (pmOS) is a pre-configured Alpine Linux that can be installed "
"on smartphones, SBC's and other mobile devices. View the [device list]"
"(https://wiki.postmarketos.org/wiki/Devices) to see the progress for "
"supporting your device."
msgstr ""
"PostmarketOS (pmOS) je vnaprej prilagojen Alpine Linux, ki ga lahko "
"namestite na pametne telefone, SBC-je in druge mobilne naprave. Oglejte si "
"[seznam naprav](https://wiki.postmarketos.org/wiki/Devices) in si oglejte "
"napredek podpore za vašo napravo."

#: content/get.md:17
msgid ""
"For devices that do not have prebuilt images, you will need to flash it "
"manually using the `pmbootstrap` utility. Follow instructions [here](https://"
"wiki.postmarketos.org/wiki/Installation_guide). Be sure to also check the "
"device's wiki page for more information on what is working."
msgstr ""
"Za naprave, ki nimajo vnaprej grajenih slik, ga boste morali zapeči ročno z "
"uporabo pripomočka »pmbootstrap«. Sledite navodilom [tukaj](https://wiki."
"postmarketos.org/wiki/Installation_guide). Če želite več informacij o tem, "
"kaj deluje, preverite tudi stran wiki za napravo."

#: content/get.md:21
msgid "[Learn more](https://postmarketos.org)"
msgstr "[Več o tem] (https://postmarketos.org)"

#: content/get.md:23 content/get.md:35
msgid "Download:"
msgstr "Prenosi:"

#: content/get.md:25
msgid "[Community Devices](https://postmarketos.org/download/)"
msgstr "[Naprave skupnosti](https://postmarketos.org/download/)"

#: content/get.md:27
msgid "Manjaro ARM"
msgstr "Manjaro ARM"

#: content/get.md:29
msgid "![](/img/manjaro.png)"
msgstr "![](/img/manjaro.png)"

#: content/get.md:31
msgid ""
"Manjaro ARM is an Arch based distribution with support for a wide range of "
"single board computers, ARM laptops and phones."
msgstr ""
"Manjaro ARM je distribucija na osnovi Arch-a s podporo za širok nabor "
"računalnikov na enem samem vezju, notesnikov s procesorjem ARM in telefonov."

#: content/get.md:33
msgid ""
"Manjaro ARM currently offers Bigscreen images for Odroid N2 and N2+, Radxa "
"Zero 2, RockPro64, Raspberry Pi 4, Khadas Vim 1 and Khadas Vim 3."
msgstr ""
"Manjaro ARM trenutno nudi Bigscreen programje za Odroid N2 in N2+, Radxa "
"Zero 2, RockPro64, Raspberry Pi 4, Khadas Vim 1 in Khadas Vim 3."

#: content/get.md:37
msgid "[Development images](https://github.com/manjaro-arm/bigscreen/releases)"
msgstr ""
"[Development images](https://github.com/manjaro-arm/bigscreen/releases)"

#: content/get.md:39
msgid "Installation"
msgstr "Namestitev"

#: content/get.md:41
msgid ""
"Simply download the Bigscreen image for the device you have, extract it so "
"you have the raw .img file, then flash it to your drive using `dd` or a "
"graphical tool. Then plug in the drive to your device and power it on."
msgstr ""
"Preprosto prenesite izvajalni program Bigscreen za vašo napravo, izluščite "
"ga, da imate datoteko raw .img, prepišite ga na vašo diskovno enoto z "
"uporabo `dd` ali grafičnega orodja. Potem priključite diskovno enoto na vašo "
"napravo in jo vključite."

#: content/get.md:43
msgid "Debian"
msgstr "Debian"

#: content/get.md:45
msgid "![](/img/debian.png)"
msgstr "![](/img/debian.png)"

#: content/get.md:47
msgid ""
"Debian has Plasma Bigscreen available as a [package ](https://packages."
"debian.org/stable/kde/plasma-bigscreen) in their repositories."
msgstr ""
"Debian ima na voljo Plasma Bigscreen kot [package ](https://packages.debian."
"org/stable/kde/plasma-bigscreen) v svojoh repozitorijih."

#: content/join.md:0
msgid "Join Plasma Bigscreen"
msgstr "Pridružite se Plasma Bigscreen"

#: content/join.md:12
msgid ""
"If you'd like to contribute to the amazing free software for TV's and setup-"
"boxes, [join us - we always have a task for you](/contributing/)!"
msgstr ""
"Če želite prispevati k neverjetni prosti programski opremi za TV-je in "
"digitalne sprejemnike, [se nam pridružite - vedno imamo nalogo za vas](/"
"contributing/)!"

#: content/join.md:14
msgid "Plasma Bigscreen specific channels:"
msgstr "Specifični kanali Plasma Bigscreen:"

#: content/join.md:16
msgid ""
"[![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasma-"
"bigscreen:kde.org)"
msgstr ""
"[![](/img/matrix.svg)Matrix (najbolj aktivno)](https://matrix.to/#/#plasma-"
"bigscreen:kde.org)"

#: content/join.md:18
msgid "[![](/img/telegram.svg)Telegram](https://t.me/plasma_bigscreen)"
msgstr "[![](/img/telegram.svg)Telegram](https://t.me/plasma_bigscreen)"

#: content/join.md:21
msgid "Plasma Biscreen related project channels:"
msgstr "S Plasma Biscreen povezani projektni kanali:"

#: content/join.md:23
msgid ""
"[![](/img/irc.png)#plasma on Freenode (IRC)](https://kiwiirc.com/nextclient/"
"chat.freenode.net/#plasma)"
msgstr ""
"[![](/img/irc.png)#plasma na Freenode (IRC)](https://kiwiirc.com/nextclient/"
"chat.freenode.net/#plasma)"

#: content/join.md:25
msgid ""
"[![](/img/mail.svg)Plasma development mailing list](https://mail.kde.org/"
"mailman/listinfo/plasma-devel)"
msgstr ""
"[![](/img/mail.svg)Dopisni seznam za pošiljanje razvoja Plasme](https://mail."
"kde.org/mailman/listinfo/plasma-devel)"

#: content/screenshots.md:0
msgid "Screenshots"
msgstr "Zaslonski zajemi"

#: content/screenshots.md:0
msgid "Plasma Bigscreen homescreen"
msgstr "Domači zaslon Plasma Bigscreen"

#: content/vision.md:0
msgid "Our Vision"
msgstr "Naša vizija"

#: content/vision.md:0
msgid "Vision"
msgstr "Vizija"

#: content/vision.md:12
msgid ""
"Plasma Bigscreen aims to become a complete and open software system for TV's "
"and setup-boxes.<br /> It is designed to give privacy-aware users back "
"control over their information, communication and media."
msgstr ""
"Plazma Bigscreen si prizadeva postati popoln in odprt programski sistem za "
"TV in digitalne sprejemnike.<br /> Zasnovan je tako, da uporabnikom, ki "
"stavijo na zasebnost, vrne nadzor nad svojimi informacijami, komunikacijo in "
"mediji."

#: content/vision.md:15
msgid ""
"Plasma Bigscreen takes a pragmatic approach and is inclusive to 3rd party "
"software, allowing the user to choose which applications and services to "
"use, while providing a seamless experience across multiple devices.<br /> "
"Plasma Bigscreen implements open standards and is developed in a transparent "
"process that is open for anyone to participate in."
msgstr ""
"Plasma Bigscreen ima pragmatični pristop in je vključujoč za programsko "
"opremo tretjih strani. Uporabniku dovoli izbrati, kateri programi in "
"storitve želi uporabljati, omogoča pa enako izkušnjo na različnih napravah."
"<br /> Plasma Bigscreen implementira odprte standarde in je razvit v "
"preglednem procesu, ki je odprt za vsakogar, da lahko sodeluje."

#: i18n/en.yaml:0
msgid "Plasma on your TV"
msgstr "Plazma na vašem televizorju"

#: i18n/en.yaml:0
msgid "This project uses various open-source components like"
msgstr "Ta projekt uporablja različne odprtokodne komponente, kot so"

#: i18n/en.yaml:0
msgid "Mycroft AI"
msgstr "Mycroft AI"

#: i18n/en.yaml:0
msgid "LibCEC"
msgstr "LibCEC"

#: i18n/en.yaml:0
msgid "Open up"
msgstr "Odprto za vse"

#: i18n/en.yaml:0
msgid ""
"Plasma Bigscreen is an open-source user interface for TV's. Running on top "
"of a Linux distribution, Plasma Bigscreen turns your TV or setup-box into a "
"fully hackable device. A big launcher giving you easy access to any "
"installed apps and skills. Controllable via voice or TV remote."
msgstr ""
"Plasma Bigscreen je odprtokodni uporabniški vmesnik za TV-je. Plasma "
"Bigscreen, ki deluje na osnovi distribucije Linuxa, spremeni televizor ali "
"zunanji digitalni sprejemnik (STB) v popolnoma prilagodljivo napravo. Velik "
"zaganjalnik vam daje enostaven dostop do vseh nameščenih aplikacij in "
"spretnosti. Krmiljenje z glasom ali TV-daljincem."

#: i18n/en.yaml:0
msgid "Contribute"
msgstr "Prispevajte"

#: i18n/en.yaml:0
msgid ""
"Using the multi-platform toolkit Qt, the flexible extensions of KDE "
"Frameworks plus the power of Plasma Shell, Plasma Bigscreen is built with "
"technology which feels equally at home on the desktop, mobile devices and TV."
msgstr ""
"S pomočjo več platformnega orodja Qt, prilagodljivih razširitev KDE "
"Frameworks ter močjo Plasma Shell je Plasma Bigscreen zgrajen s tehnologijo, "
"ki se počuti enako doma na namizju, mobilnih napravah in TV."

#: i18n/en.yaml:0
msgid "KWin and Wayland"
msgstr "KWin in Wayland"

#: i18n/en.yaml:0
msgid ""
"Wayland is the next-generation protocol for delivering cutting-edge "
"interfaces which are silky smooth. KWin is the battle-tested window manager "
"which implements Wayland, delivering a polished and reliable experience on "
"both the desktop and mobile devices."
msgstr ""
"Wayland je protokol naslednje generacije za zagotavljanje najsodobnejših "
"vmesnikov, ki tečejo gladko kot svila. KWin je v najtežjih razmerah "
"preizkušen upravljalnik oken, ki implementira Wayland, prinaša izbrušeno in "
"zanesljivo izkušnjo tako na namizju kot na mobilnih napravah."

#: i18n/en.yaml:0
msgid "Making full use of Open Source"
msgstr "Popoln izkoristek odprte kode"

#: i18n/en.yaml:0
msgid ""
"Plasma Bigscreen combines many powerful software tools from established "
"projects to make a whole greater than the sum of its parts.<br /> MyCroft "
"allows us to control our TV with just our voice, CEC makes it possible to "
"control the interface using your normal TV remote control, and Pulseaudio "
"drives the Plasma Bigscreen sound system."
msgstr ""
"Plasma Bigscreen združuje veliko zmogljivih programskih orodij iz "
"uveljavljenih projektov, kar dela celoto večjo kot je vsota njenih sestavnih "
"delov.<br /> MyCroft nam omogoča nadzor našega televizorja samo z našim "
"glasom, CEC omogoča nadzor vmesnika z običajnim TV daljinskim "
"upravljalnikom, Pulseaudio pa krmili zvočni sistem Plasma Bigscreen."

#: i18n/en.yaml:0
msgid "Adapting to your needs"
msgstr "Prilagajanje vašim potrebam"

#: i18n/en.yaml:0
msgid ""
"Being based on the most flexible desktop in the world means you can truly "
"make your TV your own. Add and modify widgets, change colour schemes, fonts, "
"and much, much more."
msgstr ""
"Ker je zasnovan na najbolj fleksibilnem namizju na svetu, pomeni, da lahko "
"resnično naredite svoj TV - svoj. Dodajanje in spreminjanje pripomočkov, "
"spreminjanje barvnih shem, pisav in še veliko več."

#: i18n/en.yaml:0
msgid "What is Plasma Bigscreen?"
msgstr "Kaj je Plasma Bigscreen?"

#: i18n/en.yaml:0
msgid ""
"Plasma Bigscreen offers a free (as in freedom and beer), user-friendly, "
"privacy-enabling and customizable platform for TV's and setup-boxes. It is "
"shipped by different distributions (ex. postmarketOS, KDE Neon), and can run "
"on the devices that are supported by the distribution."
msgstr ""
"Plasma Bigscreen ponuja prosto (kot v svobodi in pivu), uporabniku prijazno, "
"zasebnostno in prilagodljivo platformo za TV-je in zunanje digitalne "
"sprejemnike. Vključen je lahko v različne distribucije (npr. postmarketOS, "
"KDE Neon) in se lahko izvaja na napravah, ki jih distribucija podpira."

#: i18n/en.yaml:0
msgid "Why?"
msgstr "Zakaj?"

#: i18n/en.yaml:0
msgid ""
"The most common offerings on TV's and setup-boxes lack openness and trust. "
"In a world of walled gardens, we want to create a platform that respects and "
"protects the user’s privacy to the fullest. We want to provide a fully open "
"base which others can help develop and use for themselves, or in their "
"products."
msgstr ""
"Najbolj pogostim ponudbam na TV-jih in zunanjih digitalnih sprejemnikih "
"(STB) manjka odprtosti in zaupanja. V svetu obzidanih vrtov želimo ustvariti "
"platformo, ki spoštuje in varuje zasebnost uporabnika v celoti. Želimo "
"zagotoviti popolnoma odprto podlago, ki jo lahko drugi pomagajo razviti in "
"uporabiti zase ali v svojih izdelkih."

#~ msgid "Currently, Plasma Bigscreen runs on the following device types:"
#~ msgstr "Trenutno Plasma Bigscreen teče na naslednjih vrstah naprav:"

#~ msgid ""
#~ "**(Recommended) Raspberry Pi 4:** We offer official images built for the "
#~ "Raspberry Pi 4 on top of KDE Neon."
#~ msgstr ""
#~ "**(Priporočeno) Raspberry Pi 4:** Ponujamo uradne slike, zgrajene za "
#~ "Raspberry Pi 4 na KDE Neon."

#~ msgid ""
#~ "**postmarketOS devices:** postmarketOS is a distribution based on Alpine "
#~ "Linux that can be installed on Android smartphones but also on SBC's like "
#~ "the Raspberry Pi 4 and other devices. Please find your device from the "
#~ "[list of supported devices](https://wiki.postmarketos.org/wiki/Devices) "
#~ "and see what's working, then you can follow the [pmOS installation guide]"
#~ "(https://wiki.postmarketos.org/wiki/Installation_guide) to install it on "
#~ "your device. Your mileage may vary depending on the device in use and "
#~ "when you use a device in the testing category it is **not** necessarily "
#~ "representative of the current state of Plasma Bigscreen."
#~ msgstr ""
#~ "**Naprave postmarketOS:** postmarketOS je distribucija, ki temelji na "
#~ "Alpine Linux, ki jo lahko namestimo na Android pametnih telefonov, pa "
#~ "tudi na SBC-je, kakršen je Raspberry Pi 4 in druge naprave. Poiščite "
#~ "napravo s [seznama podprtih naprav](https://wiki.postmarketos.org/wiki/"
#~ "Devices) in si oglejte, kaj deluje, potem lahko sledite [namestitvenem "
#~ "vodniku pmOS](https://wiki.postmarketos.org/wiki/Installation_guide), da "
#~ "ga namestite na svojo napravo. Vaša spretnost se lahko razlikuje glede na "
#~ "napravo, ki je v uporabi, in kadar uporabljate napravo v kategoriji "
#~ "testiranja, to **ni** nujno reprezentativno za trenutno stanje Plasma "
#~ "Bigscreen."

#~ msgid "I've installed Plasma Bigscreen, what is the login password?"
#~ msgstr "Namestil/a sem Plasma Bigscreen, kakšno je geslo za prijavo?"

#~ msgid ""
#~ "If you've installed the reference KDE Neon image for the Raspberry Pi 4, "
#~ "the password and username are both \"mycroft\", and you can then change "
#~ "the password afterwards by running \"passwd\" from a tty."
#~ msgstr ""
#~ "Če ste namestili referenčno sliko KDE Neon za Raspberry Pi 4, sta geslo "
#~ "in uporabniško ime tako »mycroft«, nato pa lahko spremenite geslo z "
#~ "zagonom »passwd« iz tty."

#~ msgid "Neon based reference rootfs"
#~ msgstr "Na Neonu temelječi referenčni rootfs"

#~ msgid "![](/img/neon.svg)"
#~ msgstr "! [](/img/neon.svg)"

#~ msgid ""
#~ "Image based on KDE Neon. KDE Neon itself is based upon Ubuntu 20.04 "
#~ "(focal). This image is based on the dev-unstable branch of KDE Neon, and "
#~ "always ships the latest versions of KDE frameworks, KWin and Plasma "
#~ "Bigscreen compiled from git master."
#~ msgstr ""
#~ "Slika, ki temelji na KDE Neonu. KDE Neon sam temelji na Ubuntu 20.04 "
#~ "(focal). Ta slika temelji na veji dev-unstable KDE Neon in vedno pride z "
#~ "najnovejšo različico KDE Frameworks, KWin in Plasma Bigscreen, prevedenih "
#~ "iz mastra git."

#~ msgid ""
#~ "[Raspberry Pi 4](https://sourceforge.net/projects/bigscreen/files/rpi4/"
#~ "beta)"
#~ msgstr ""
#~ "[Raspberry Pi 4](https://sourceforge.net/projects/bigscreen/files/rpi4/"
#~ "beta)"

#~ msgid ""
#~ "Download the image, uncompress it and flash it to a SD-card using `dd` or "
#~ "a graphical tool. The Raspberry Pi will automatically boot from a SD-Card."
#~ msgstr ""
#~ "Prenesite sliko, jo razširite in zapecite na kartico SD z uporabo ukaza "
#~ "»dd« ali grafičnega orodja. Raspberry Pi se bo samodejno zagnal iz SD-"
#~ "kartice."

#~ msgid "For a complete step-by-step readme, [click here](/manual)."
#~ msgstr "Za branje celote po korakih [kliknite tukaj](/manual)."
